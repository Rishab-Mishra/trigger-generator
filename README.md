# Trigger Generator
### This is a project to generate new triggers and automatically copy-to-clipboard
### The fields which are modified are triggerId, triggerTs in main payload and also with extractParams
****

## Prequisites
Use [Node v12](https://nodejs.org/en/blog/release/v12.13.0/)

### `npm i`

To install required dependencies
***
### `npm start`

Runs the app in the development mode.  
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

>The page will reload if you make edits.  
>You will also see any lint errors in the console.
***
## DOCKER COMMANDS

To build and create a container to directly run using docker and just avoid using a IDE/ Code editor:  
`docker build -t generate-trigger .` (the . at end is required)  
`docker run --name trigger-gen -p 3000:80 -d generate-trigger`
***