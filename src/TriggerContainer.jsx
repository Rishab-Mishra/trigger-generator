import React from 'react';
import { TextField, Button } from '@material-ui/core';

class NameForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { value: '' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        alert('A name was submitted: ' + this.state.value);
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <TextField>
                    Name:
            <input type="text" value={this.state.value} onChange={this.handleChange} />
                </TextField>
                <Button
                    variant="contained" color="primary">
                    Generate
                    </Button>
                <Button type="submit" value="Submit"></Button>
            </form>
        );
    }
}

export default NameForm;

{ "triggerType": "FordUdbQuoteTrigger", "triggerId": "ea2cd10a-b54d-4a0e-a219-044f8c4dcdc9", "triggerTs": "2020-05-01T01:40:53.769Z", "nextTriggerTs": null, "originatingApp": "mds-dataextract-engine", "originatingUser": "mds-dataextract-engine", "ingestProvider": { "providerId": "e08f3dbf-2b7d-40a1-b19e-1ca3b70fa58d", "providerName": "Ford UDB" }, "interval": null, "flags": [], "extractParams": { "triggerTs": "2020-05-01T01:40:53.769Z", "siteCode": "USA02737 ", "subCode": " ", "countryCode": "USA", "triggerId": "8d87f252-e777-4a9f-97c4-39336ea225ac" }, "dealerId": "02737", "feedType": "PartQuote" }
{ "triggerType": "FordUdbQuoteTrigger", "triggerId": "66a410e1-4418-44d8-8759-73a7063ee6bb", "triggerTs": "2020-05-01T01:42:00.629Z", "nextTriggerTs": null, "originatingApp": "mds-dataextract-engine", "originatingUser": "mds-dataextract-engine", "ingestProvider": { "providerId": "e08f3dbf-2b7d-40a1-b19e-1ca3b70fa58d", "providerName": "Ford UDB" }, "interval": null, "flags": [], "extractParams": { "triggerTs": "2020-05-01T01:42:00.629Z", "siteCode": "USA02737 ", "subCode": " ", "countryCode": "USA", "triggerId": "8d87f252-e777-4a9f-97c4-39336ea225ac" }, "dealerId": "02737", "feedType": "PartQuote" }