import React from 'react';
import './App.css';

import FilledTextFields from './FilledTextFields';

//https://v3.material-ui.com/demos/text-fields/

function App() {
  return (
    <div className="App">
      <FilledTextFields />
    </div>
  );
}

export default App;
