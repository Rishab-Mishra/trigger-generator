import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { v4 as uuidv4 } from 'uuid';
import copy from 'copy-to-clipboard';

import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

import Button from '@material-ui/core/Button';

const styles = theme => ({

    dense: {
        marginTop: 16,
    }
});

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class FilledTextFields extends React.Component {
    state = {
        open: false,
        error: false,
        value: ''
    };

    isJSON(str) {
        try {
            return JSON.parse(str) && !!str;
        } catch (e) {
            return false;
        }
    }

    handleClick = () => {
        this.setState({ open: true });
    };

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({ open: false });
    };

    handleSubmit = (e) => {
        const trigger = JSON.parse(this.state.value)
        const triggerTs = new Date(Date.now()).toISOString()
        const triggerId = uuidv4()
        const extractParams = Object.assign({}, trigger.extractParams, { triggerTs, triggerId })
        const overlapping = {
            triggerId,
            triggerTs,
            extractParams
        }

        const updatedTrigger = Object.assign({}, trigger, overlapping)

        const value = JSON.stringify(updatedTrigger)
        copy(value)
        this.setState({ copied: true, open: true, value })
        e.preventDefault();
    }

    handleChange = (event) => {
        const error = !this.isJSON(event.target.value)
        this.setState({
            error,
            value: event.target.value,
            copied: false
        })
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                <form onSubmit={this.handleSubmit} autoComplete="off">
                    <div><TextField
                        size='medium'
                        id="outlined-textarea"
                        label="Trigger"
                        placeholder="Paste a trigger here to get updated triggedId and triggerTs on each generate"
                        value={this.state.value}
                        error={this.state.error}
                        helperText={this.state.error ? "Invalid JSON" : ""}
                        multiline
                        fullWidth
                        className={classes.textField}
                        margin='normal'
                        variant="outlined"
                        onChange={this.handleChange}
                    />
                    </div>
                    <Button
                        disabled={this.state.error}
                        type='submit'
                        variant="contained" color="primary">
                        Generate
                    </Button>
                    <Snackbar open={this.state.open} autoHideDuration={4000} onClose={this.handleClose}>
                        <Alert onClose={this.handleClose} severity="success">
                            New trigger copied to clipboard!
                        </Alert>
                    </Snackbar>
                </form>
            </div>
        );
    }
}

FilledTextFields.propTypes = {
    classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(FilledTextFields);
